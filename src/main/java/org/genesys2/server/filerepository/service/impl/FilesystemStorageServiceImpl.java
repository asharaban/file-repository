/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.genesys2.server.filerepository.BytesStorageException;
import org.genesys2.server.filerepository.service.BytesStorageService;
import org.springframework.stereotype.Service;

// TODO: Auto-generated Javadoc
/**
 * The Class FilesystemStorageServiceImpl.
 *
 * @author mobreza
 */
@Service("fileSystemStorage")
public class FilesystemStorageServiceImpl implements BytesStorageService {

	/** Repository base directory. */
	private File repoDir;

	/**
	 * Sets the repository base directory.
	 *
	 * @param repoDir the new repository base directory
	 */
	public void setRepositoryBaseDirectory(final File repoDir) {
		this.repoDir = repoDir;
	}

	/**
	 * After properties set.
	 *
	 * @throws BytesStorageException the bytes storage exception
	 */
	public void afterPropertiesSet() throws BytesStorageException {
		sanityCheck();
	}

	/**
	 * Sanity check.
	 *
	 * @throws BytesStorageException the bytes storage exception
	 */
	// Do not do any null checks, let it fail.
	protected void sanityCheck() throws BytesStorageException {
		if (!repoDir.isDirectory()) {
			throw new BytesStorageException("Base path is not a directory");
		}

		// Base directory must contain the file FILEREPOSITORY (so we don't
		// create things all over the place)
		final File repositoryMarker = new File(repoDir, "FILEREPOSITORY");
		if (!repositoryMarker.exists() || !repositoryMarker.isFile()) {
			throw new BytesStorageException("Missing FILEREPOSITORY marker in " + repoDir.getAbsolutePath());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.BytesStorageService#upsert(java.lang.String, java.lang.String,
	 * byte[])
	 */
	@Override
	public void upsert(final String path, final String filename, final byte[] data) throws FileNotFoundException,
			IOException {
		final File destinationDir = new File(repoDir, path);

		if (!destinationDir.getCanonicalPath().startsWith(repoDir.getCanonicalPath())) {
			throw new IOException("Not within repository path: " + destinationDir.getAbsolutePath());
		} else if (!destinationDir.exists()) {
			destinationDir.mkdirs();
		} else if (!destinationDir.isDirectory()) {
			throw new IOException("Exists, not a directory: " + destinationDir.getAbsolutePath());
		}

		final File destinationFile = new File(destinationDir, filename);

		try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(destinationFile, false))) {
			IOUtils.write(data, output);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.BytesStorageService#remove(java.lang.String, java.lang.String)
	 */
	@Override
	public void remove(final String path, final String filename) throws IOException {
		final File destinationDir = new File(repoDir, path);
		final File destinationFile = new File(destinationDir, filename);

		if (!destinationDir.getCanonicalPath().startsWith(repoDir.getCanonicalPath())) {
			throw new IOException("Not within repository path: " + destinationDir.getAbsolutePath());
		}

		if (destinationFile.exists()) {
			destinationFile.delete();
		}

		// TODO FIXME Delete or not?
		// Delete empty dir
		if (destinationDir.list().length == 0) {
			// System.err.println("DELETING " +
			// destinationDir.getAbsolutePath());
			destinationDir.delete();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.BytesStorageService#get(java.lang.String, java.lang.String)
	 */
	@Override
	public byte[] get(final String path, final String filename) throws IOException {
		final File destinationDir = new File(repoDir, path);
		final File destinationFile = new File(destinationDir, filename);

		if (!destinationDir.getCanonicalPath().startsWith(repoDir.getCanonicalPath())) {
			throw new IOException("Not within repository path: " + destinationDir.getAbsolutePath());
		}

		byte[] data = null;
		if (destinationFile.exists()) {
			try (FileInputStream inputStream = new FileInputStream(destinationFile)) {
				data = IOUtils.toByteArray(inputStream);
			}
		}

		return data;
	}
}

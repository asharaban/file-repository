/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.genesys2.server.filerepository.service.BytesStorageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

/**
 * Amazon S3 storage implementation
 */
@Service("S3Storage")
public class S3StorageServiceImpl implements BytesStorageService {

	private static final SimpleDateFormat HEADER_DATE_FORMAT = new SimpleDateFormat(
			"EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);

	private final RestTemplate restTemplate = initializeRestTemplate();

	@Value("${s3.accessKey}")
	private String accessKey;

	@Value("${s3.secretKey}")
	private String secretKey;

	@Value("${s3.bucket}")
	private String bucket;

	@Value("${s3.region}")
	private String region;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.BytesStorageService#upsert (java.lang.String, java.lang.String,
	 * byte[])
	 */
	@Override
	public void upsert(final String path, final String filename, final byte[] data) throws FileNotFoundException,
			IOException {
		if (!isValidPath(path)) {
			throw new IOException("Path is not valid");
		}

		if (filename == null) {
			throw new IOException("File name is null");
		}

		if (data == null) {
			throw new IOException("File bytes are null");
		}

		final String url = getUrl(path, filename);
		restTemplate.put(url, data);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.BytesStorageService#remove (java.lang.String, java.lang.String)
	 */
	@Override
	public void remove(final String path, final String filename) throws IOException {
		if (!isValidPath(path)) {
			throw new IOException("Path is not valid");
		}

		if (filename == null) {
			throw new IOException("File name is null");
		}

		final String url = getUrl(path, filename);
		restTemplate.delete(url);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.BytesStorageService#get(java .lang.String, java.lang.String)
	 */
	@Override
	public byte[] get(final String path, final String filename) throws IOException {
		if (!isValidPath(path)) {
			throw new IOException("Path is not valid");
		}

		if (filename == null) {
			throw new IOException("File name is null");
		}

		final String url = getUrl(path, filename);
		return restTemplate.getForObject(url, byte[].class);
	}

	/**
	 * Returns URL for S3 resource.
	 *
	 * @param path
	 * @param filename
	 * @return
	 */
	private String getUrl(final String path, final String filename) {
		return String.format("http://%s%s", getHost(), path + filename);
	}

	/**
	 * Get the hostname part of the S3 resource URL
	 *
	 * @return
	 */
	private String getHost() {
		return String.format("%s.s3-%s.amazonaws.com", bucket, region);
	}

	/**
	 * Returns string to sign as specified at http://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#
	 * ConstructingTheAuthenticationHeader
	 *
	 * @param method
	 * @param date
	 * @param mediaType
	 * @param path
	 * @param filename
	 * @return
	 */
	private String getStringToSign(final String method, final String date, final String filePath,
			final MediaType mediaType) {

		// HTTP-Verb
		final StringBuilder sb = new StringBuilder(method).append("\n");

		// Content MD5
		sb.append("\n");

		// Content type
		if (mediaType != null) {
			sb.append(mediaType.toString());
		}

		sb.append("\n");

		// Timestamp
		sb.append(date).append("\n");

		// CanonicalizedResource
		sb.append("/").append(bucket).append(filePath);

		return sb.toString();
	}

	/**
	 * Returns AWS authorization HTTP Header
	 *
	 * @param signature
	 * @return
	 */
	private String getAuthorizationHeader(final String signature) {
		return new StringBuilder("AWS").append(" ").append(accessKey).append(":").append(signature).toString();
	}

	/**
	 * Returns encrypted signature.
	 *
	 * @param stringToSign
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 */
	private byte[] encryptHmacSHA1(final String stringToSign) throws NoSuchAlgorithmException, InvalidKeyException,
			UnsupportedEncodingException {
		final String algorithm = "HmacSHA1";
		final Mac mac = Mac.getInstance(algorithm);
		mac.init(new SecretKeySpec(secretKey.getBytes("UTF8"), algorithm));
		return mac.doFinal(stringToSign.getBytes("UTF8"));
	}

	/**
	 * Checks if path is valid.
	 *
	 * @param path the path
	 * @return true, if path is valid
	 */
	private boolean isValidPath(final String path) {
		return path != null && !StringUtils.containsWhitespace(path) && path.startsWith("/") && path.endsWith("/")
				&& !path.contains("//");
	}

	/**
	 * Initializes RestTemplate with the interceptor that signs the HTTP requests to AWS
	 *
	 * @return
	 */
	private RestTemplate initializeRestTemplate() {
		final RestTemplate restTemplate = new RestTemplate();

		final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new ClientHttpRequestInterceptor() {

			/**
			 * Add AWS authorization headers
			 */
			@Override
			public ClientHttpResponse intercept(final HttpRequest request, final byte[] body,
					final ClientHttpRequestExecution execution) throws IOException {

				final String date = HEADER_DATE_FORMAT.format(new Date());
				request.getHeaders().set("Date", date);
				request.getHeaders().set("Host", getHost());

				final String stringToSign = getStringToSign(request.getMethod().toString(), date, request.getURI()
						.getPath(), request.getHeaders().getContentType());

				String authorizationHeader = null;
				try {
					final byte[] signatureBytes = encryptHmacSHA1(stringToSign);
					final String signature = new String(Base64.getEncoder().encode(signatureBytes));
					authorizationHeader = getAuthorizationHeader(signature);
				} catch (NoSuchAlgorithmException | InvalidKeyException e) {
					throw new IOException("Signature for request can't be created");
				}

				request.getHeaders().set("Authorization", authorizationHeader);
				return execution.execute(request, body);
			}
		});
		restTemplate.setInterceptors(interceptors);

		return restTemplate;
	}
}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import java.io.FileNotFoundException;
import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Interface BytesStorageService.
 */
public interface BytesStorageService {

	/**
	 * Upsert.
	 *
	 * @param path the path
	 * @param filename the filename
	 * @param data the data
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void upsert(String path, String filename, byte[] data) throws FileNotFoundException, IOException;

	/**
	 * Removes the.
	 *
	 * @param path the path
	 * @param filename the filename
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	void remove(String path, String filename) throws IOException;

	/**
	 * Gets the file bytes.
	 *
	 * @param path the path
	 * @param filename the filename
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	byte[] get(String path, String filename) throws IOException;

}

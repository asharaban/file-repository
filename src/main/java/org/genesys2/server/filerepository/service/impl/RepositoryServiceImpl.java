/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.genesys2.server.filerepository.persistence.RepositoryDocumentPersistence;
import org.genesys2.server.filerepository.persistence.RepositoryFilePersistence;
import org.genesys2.server.filerepository.persistence.RepositoryImagePersistence;
import org.genesys2.server.filerepository.service.BytesStorageService;
import org.genesys2.server.filerepository.service.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class RepositoryServiceImpl.
 */
@Service
public class RepositoryServiceImpl implements RepositoryService {

	/** The document file persistence. */
	@Autowired
	private RepositoryDocumentPersistence documentFilePersistence;

	/** The repository file persistence. */
	@Autowired
	private RepositoryFilePersistence repositoryFilePersistence;

	/** The repository image persistence. */
	@Autowired
	private RepositoryImagePersistence repositoryImagePersistence;

	/** The bytes storage service. */
	@Autowired
	private BytesStorageService bytesStorageService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.RepositoryService#addFile(java.lang.String, java.lang.String,
	 * java.lang.String, byte[], org.genesys2.server.filerepository.model.RepositoryFile)
	 */
	@Override
	public RepositoryFile addFile(final String repositoryPath, final String originalFilename, final String mimeType,
			final byte[] bytes, final RepositoryFile metaData) throws InvalidRepositoryPathException,
			InvalidRepositoryFileDataException {
		if (!isValidPath(repositoryPath)) {
			throw new InvalidRepositoryPathException();
		}

		if (originalFilename == null || mimeType == null || bytes == null) {
			throw new InvalidRepositoryFileDataException();
		}

		RepositoryFile file = new RepositoryFile();

		if (metaData != null) {
			copyMetaData(metaData, file);
		}

		file.setOriginalFilename(originalFilename);
		file.setMimeType(mimeType);
		file.setPath(repositoryPath);

		file = repositoryFilePersistence.save(file);

		try {
			bytesStorageService.upsert(file.getPath(), file.getFilename(), bytes);
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return file;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.RepositoryService#addImage(java.lang.String, java.lang.String,
	 * java.lang.String, byte[], org.genesys2.server.filerepository.model.RepositoryImage)
	 */
	@Override
	public RepositoryImage addImage(final String repositoryPath, final String originalFilename, final String mimeType,
			final byte[] bytes, final RepositoryImage metaData) throws InvalidRepositoryPathException,
			InvalidRepositoryFileDataException {
		if (!isValidPath(repositoryPath)) {
			throw new InvalidRepositoryPathException();
		}

		if (originalFilename == null || mimeType == null || bytes == null) {
			throw new InvalidRepositoryFileDataException();
		}

		RepositoryImage image = new RepositoryImage();

		if (metaData != null) {
			copyMetaData(metaData, image);
		}

		image.setPath(repositoryPath);
		image.setOriginalFilename(originalFilename);
		image.setMimeType(mimeType);

		try {
			final InputStream in = new ByteArrayInputStream(bytes);
			final BufferedImage imageData = ImageIO.read(in);

			if (imageData != null) {
				image.setWidth(imageData.getWidth());
				image.setHeight(imageData.getHeight());
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}

		image = repositoryImagePersistence.save(image);

		try {
			bytesStorageService.upsert(image.getPath(), image.getFilename(), bytes);
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.RepositoryService#getFile(java.util.UUID)
	 */
	@Override
	public RepositoryFile getFile(final UUID fileUuid) throws NoSuchRepositoryFileException {
		RepositoryFile file = repositoryFilePersistence.findByUuid(fileUuid);
		if (file != null) {
			return file;
		}

		file = repositoryImagePersistence.findByUuid(fileUuid);
		if (file == null) {
			throw new NoSuchRepositoryFileException();
		}

		return file;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.RepositoryService#getFileBytes(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public byte[] getFileBytes(final String repositoryPath, final String originalFilename)
			throws NoSuchRepositoryFileException {
		byte[] data = null;

		try {
			data = bytesStorageService.get(repositoryPath, originalFilename);
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.service.RepositoryService#getFiles(java.lang.String)
	 */
	@Override
	public List<RepositoryFile> getFiles(final String repositoryPath) {
		final List<RepositoryFile> repositoryFiles = new ArrayList<RepositoryFile>();
		repositoryFiles.addAll(repositoryFilePersistence.findByPath(repositoryPath));
		repositoryFiles.addAll(repositoryImagePersistence.findByPath(repositoryPath));

		return repositoryFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.genesys2.server.filerepository.service.RepositoryService#updateFile(org.genesys2.server.filerepository.model
	 * .RepositoryFile)
	 */
	@Override
	public RepositoryFile updateFile(final RepositoryFile fileData) throws NoSuchRepositoryFileException {
		if (fileData == null) {
			throw new NoSuchRepositoryFileException();
		}

		RepositoryFile repositoryFile;
		if (fileData.getMimeType().startsWith("image")) {
			repositoryFile = repositoryImagePersistence.save((RepositoryImage) fileData);
		} else {
			repositoryFile = repositoryFilePersistence.save(fileData);
		}

		return repositoryFile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.genesys2.server.filerepository.service.RepositoryService#updateBytes(org.genesys2.server.filerepository.model
	 * .RepositoryFile, java.lang.String, byte[])
	 */
	@Override
	public RepositoryFile updateBytes(final RepositoryFile fileData, final String mimeType, final byte[] data)
			throws NoSuchRepositoryFileException, IOException {
		if (fileData == null) {
			throw new NoSuchRepositoryFileException();
		}

		fileData.setMimeType(mimeType);
		bytesStorageService.upsert(fileData.getPath(), fileData.getFilename(), data);

		return repositoryFilePersistence.save(fileData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.genesys2.server.filerepository.service.RepositoryService#updateBytes(org.genesys2.server.filerepository.model
	 * .RepositoryImage, java.lang.String, byte[])
	 */
	@Override
	public RepositoryImage updateBytes(final RepositoryImage imageData, final String mimeType, final byte[] data)
			throws NoSuchRepositoryFileException, IOException {
		if (imageData == null) {
			throw new NoSuchRepositoryFileException();
		}

		imageData.setMimeType(mimeType);

		final InputStream in = new ByteArrayInputStream(data);
		final BufferedImage image = ImageIO.read(in);

		if (image != null) {
			imageData.setWidth(image.getWidth());
			imageData.setHeight(image.getHeight());
		}

		bytesStorageService.upsert(imageData.getPath(), imageData.getFilename(), data);

		return repositoryImagePersistence.save(imageData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.genesys2.server.filerepository.service.RepositoryService#removeFile(org.genesys2.server.filerepository.model
	 * .RepositoryFile)
	 */
	@Override
	public RepositoryFile removeFile(final RepositoryFile repositoryFile) throws NoSuchRepositoryFileException,
			IOException {
		if (repositoryFile == null) {
			throw new NoSuchRepositoryFileException();
		}

		bytesStorageService.remove(repositoryFile.getPath(), repositoryFile.getFilename());

		if (repositoryFile.getMimeType().startsWith("image")) {
			repositoryImagePersistence.delete((RepositoryImage) repositoryFile);
		} else {
			repositoryFilePersistence.delete(repositoryFile);
		}

		return repositoryFile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.genesys2.server.filerepository.service.RepositoryService#moveFile(org.genesys2.server.filerepository.model
	 * .RepositoryFile, java.lang.String)
	 */
	@Override
	public RepositoryFile moveFile(final RepositoryFile repositoryFile, final String newPath)
			throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		if (!isValidPath(newPath)) {
			throw new InvalidRepositoryPathException();
		}

		if (repositoryFile == null) {
			throw new NoSuchRepositoryFileException();
		}

		repositoryFile.setPath(newPath);

		RepositoryFile file;
		if (repositoryFile.getMimeType().startsWith("image")) {
			file = repositoryImagePersistence.save((RepositoryImage) repositoryFile);
		} else {
			file = repositoryFilePersistence.save(repositoryFile);
		}

		return file;
	}

	/**
	 * Copy meta data.
	 *
	 * @param sourceFile the source file
	 * @param resultFile the result file
	 */
	private void copyMetaData(final RepositoryFile sourceFile, final RepositoryFile resultFile) {
		resultFile.setMimeType(sourceFile.getMimeType());
		resultFile.setDescription(sourceFile.getDescription());
		resultFile.setOriginalFilename(sourceFile.getOriginalFilename());
		resultFile.setAccessRights(sourceFile.getAccessRights());
		resultFile.setBibliographicCitation(sourceFile.getBibliographicCitation());
		resultFile.setCreated(sourceFile.getCreated());
		resultFile.setCreatedDate(sourceFile.getCreatedDate());
		resultFile.setCreator(sourceFile.getCreator());
		resultFile.setExtent(sourceFile.getExtent());
		resultFile.setLastModifiedDate(sourceFile.getLastModifiedDate());
		resultFile.setLicense(sourceFile.getLicense());
		resultFile.setPath(sourceFile.getPath());
		resultFile.setRightsHolder(sourceFile.getRightsHolder());
		resultFile.setSubject(sourceFile.getSubject());
		resultFile.setTitle(sourceFile.getTitle());
	}

	/**
	 * Checks if is valid path.
	 *
	 * @param path the path
	 * @return true, if is valid path
	 */
	private boolean isValidPath(final String path) {
		return path != null && !StringUtils.containsWhitespace(path) && path.startsWith("/") && path.endsWith("/")
				&& !path.contains("//");
	}

}

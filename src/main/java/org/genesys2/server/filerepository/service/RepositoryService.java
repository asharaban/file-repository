/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.metadata.ImageMetadata;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.genesys2.server.filerepository.model.RepositoryImage;

/**
 * The Interface RepositoryService.
 */
public interface RepositoryService {

	/**
	 * Add a new file to the file repository.
	 *
	 * @param repositoryPath the repository path
	 * @param originalFilename the original filename
	 * @param mimeType the mime type
	 * @param bytes the bytes
	 * @param metaData the meta data
	 * @return the repository file
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	RepositoryFile addFile(String repositoryPath, String originalFilename, String mimeType, byte[] bytes,
			RepositoryFile metaData) throws InvalidRepositoryPathException, InvalidRepositoryFileDataException;

	/**
	 * Add a new image to the file repository.
	 *
	 * @param repositoryPath the repository path
	 * @param originalFilename the original filename
	 * @param mimeType the mime type
	 * @param bytes the bytes
	 * @param metaData the meta data
	 * @return the repository image
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	RepositoryImage addImage(String repositoryPath, String originalFilename, String mimeType, byte[] bytes,
			RepositoryImage metaData) throws InvalidRepositoryPathException, InvalidRepositoryFileDataException;

	/**
	 * Get repository file by its UUID.
	 *
	 * @param fileUuid the file uuid
	 * @return the file
	 * @throws NoSuchRepositoryFileException when file is not available in the repository
	 */
	RepositoryFile getFile(UUID fileUuid) throws NoSuchRepositoryFileException;

	/**
	 * Get repository file bytes by its path and filename.
	 *
	 * @param repositoryPath path to the file
	 * @param originalFilename name of the file
	 * @throws NoSuchRepositoryFileException
	 */
	byte[] getFileBytes(String repositoryPath, String originalFilename) throws NoSuchRepositoryFileException;

	/**
	 * List all files at the specified repository path.
	 *
	 * @param repositoryPath the repository path
	 * @return the files
	 */
	List<RepositoryFile> getFiles(String repositoryPath);

	/**
	 * Update file metadata. The update is based on the record UUID.
	 *
	 * @param fileData the file data
	 * @return the updated RepositoryFile
	 * @throws NoSuchRepositoryFileException when file is not available in the repository
	 */
	RepositoryFile updateFile(RepositoryFile fileData) throws NoSuchRepositoryFileException;

	/**
	 * Update file bytes.
	 *
	 * @param fileData the file data
	 * @param mimeType the mime type
	 * @param data the data
	 * @return the repository file
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	RepositoryFile updateBytes(RepositoryFile fileData, String mimeType, byte[] data)
			throws NoSuchRepositoryFileException, IOException;

	/**
	 * Load image to validate data and mimeType, update image bytes and generate updated {@link ImageMetadata}.
	 *
	 * @param imageData the image data
	 * @param mimeType the mime type
	 * @param data the data
	 * @return the repository image
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	RepositoryImage updateBytes(RepositoryImage imageData, String mimeType, byte[] data)
			throws NoSuchRepositoryFileException, IOException;

	/**
	 * Remove file and its data from repository.
	 *
	 * @param repositoryFile the repository file
	 * @return the deleted entity
	 * @throws NoSuchRepositoryFileException when file is not available in the repository
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	RepositoryFile removeFile(RepositoryFile repositoryFile) throws NoSuchRepositoryFileException, IOException;

	/**
	 * Move the file to a different repository path.
	 *
	 * @param repositoryFile the repository file
	 * @param newPath the new path
	 * @return the repository file
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException when the new path is invalid
	 */
	RepositoryFile moveFile(RepositoryFile repositoryFile, String newPath) throws NoSuchRepositoryFileException,
			InvalidRepositoryPathException;

}

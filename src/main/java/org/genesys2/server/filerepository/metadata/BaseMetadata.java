/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.metadata;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * Basic DublinCore metadata for repository files.
 *
 * @author mobreza
 *
 */
public interface BaseMetadata {

	/**
	 * Get the permanent unique identifier of the resource. Commonly a Uniform Resource Locator (URL).
	 *
	 * @return the identifier
	 */
	String getIdentifier();

	/**
	 * Title is a property that refers to the name or names by which a resource is formally known. It may be qualified
	 * and repeated. Commonly used in conjunction with dc:alternative.
	 *
	 * @return the title
	 */
	String getTitle();

	/**
	 * The Subject described or pictured in the resource.
	 *
	 * @return the subject
	 */
	String getSubject();

	/**
	 * This property refers to the description of the content of a resource. The description is a potentially rich
	 * source of indexable terms and assist the users in their selection of an appropriate resource.
	 *
	 * @return the description
	 */
	String getDescription();

	/**
	 * An entity primarily responsible for making the resource.
	 *
	 * @return the creator
	 */
	String getCreator();

	/**
	 * A point or period of time when the resource was created by the {@link #getCreator()}. This is not a {@link Date}
	 * type to allow for text like "Jan - Feb 2016".
	 *
	 * @return the created
	 */
	String getCreated();

	/**
	 * Relationship between the resource and a person or an organization owning or managing rights over this resource.
	 *
	 * @return the rights holder
	 */
	String getRightsHolder();

	/**
	 * Access rights provides information about restrictions to view, search or use a resource based on attributes of
	 * the resource itself or the category of user. E.g.: My colleagues only
	 *
	 * @return the access rights
	 */
	String getAccessRights();

	/**
	 * Legal document giving official permission to do something with the resource. E.g.
	 * http://www.gnu.org/licenses/gpl.html
	 *
	 * @return the license
	 */
	String getLicense();

	/**
	 * The MIME type of the resource.
	 *
	 * @return the format
	 */
	String getFormat();

	/**
	 * Size (e.g. bytes, pages, inches, etc.) or duration (e.g. hours, minutes, days, etc.) of a resource.
	 *
	 * @return the extent
	 */
	String getExtent();

	/**
	 * Formal bibliographic citation for the resource.
	 *
	 * @return the bibliographic citation
	 */
	String getBibliographicCitation();

	/**
	 * Date when the resource was submitted to the file repository.
	 *
	 * @return the date submitted
	 */
	Date getDateSubmitted();

	/**
	 * Date when the resource was last modified in the file repository.
	 *
	 * @return the modified
	 */
	Date getModified();

}

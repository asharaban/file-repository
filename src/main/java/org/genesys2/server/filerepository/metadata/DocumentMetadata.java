/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.metadata;

// TODO: Auto-generated Javadoc
/**
 * The Interface DocumentMetadata.
 */
public interface DocumentMetadata extends BaseMetadata {

	/**
	 * The language of the intellectual content of the resource. Best practice would be to select a value from the three
	 * letter language tags of ISO639.
	 *
	 * @return the language
	 */
	String getLanguage();

	/**
	 * Property is used when the description of a resource is a formal abstract.
	 *
	 * @return the abstract
	 */
	String getAbstract();
}

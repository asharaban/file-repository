/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.metadata;

// TODO: Auto-generated Javadoc
/**
 * The Interface ImageMetadata.
 */
public interface ImageMetadata extends BaseMetadata {

	/**
	 * Gets the width.
	 *
	 * @return image width in pixels.
	 */
	int getWidth();

	/**
	 * Gets the height.
	 *
	 * @return image height in pixels.
	 */
	int getHeight();

	/**
	 * Get image orientation. Landscape = width is greater than height. Portrait = width is less or equal height;
	 *
	 * @return the orientation
	 */
	Orientation getOrientation();

	/**
	 * The Enum Orientation.
	 */
	public static enum Orientation {

		/** The portrait. */
		PORTRAIT,
		/** The landscape. */
		LANDSCAPE;
	}
}

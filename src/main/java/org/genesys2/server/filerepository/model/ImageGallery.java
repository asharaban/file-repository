/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.model;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * {@link ImageGallery} is a collection of ordered {@link RepositoryImage} instances. The images can be owned by anyone.
 * Gallery has a dedicated "path" in the file repository, where new images get posted. When an image is removed from the
 * Gallery, it may be removed if it is hosted in the Gallery path, but must be kept as-is if it does not share the path.
 *
 * FIXME Do we need this thing right now?
 *
 * @author mobreza
 *
 */
public class ImageGallery {

	/** The images. */
	private List<RepositoryImage> images;

	/**
	 * Gets the images.
	 *
	 * @return the images
	 */
	public List<RepositoryImage> getImages() {
		return images;
	}

	/**
	 * Sets the images.
	 *
	 * @param images the new images
	 */
	public void setImages(final List<RepositoryImage> images) {
		this.images = images;
	}
}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.filerepository.metadata.DocumentMetadata;
import org.genesys2.server.filerepository.metadata.ImageMetadata;

// TODO: Auto-generated Javadoc
/**
 * An {@link RepositoryDocument} is an graphics file in one of the supported image formats (PNG and JPG). It extends the
 * {@link RepositoryFile} by including image-specific metadata defined in {@link ImageMetadata}.
 *
 * @author mobreza
 */
@Entity
@Table(name = "repositorydocument")
public class RepositoryDocument extends RepositoryFile implements DocumentMetadata {

	/** The language. */
	@Column(name = "language")
	private String language;

	/** The abstrct. */
	@Column(name = "abstract")
	private String abstrct;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.model.RepositoryFile#prePersist()
	 */
	@Override
	@PrePersist
	protected void prePersist() {
		// Don't forget the superclass!
		super.prePersist();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.DocumentMetadata#getLanguage()
	 */
	@Override
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(final String language) {
		this.language = language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.DocumentMetadata#getAbstract()
	 */
	@Override
	public String getAbstract() {
		return abstrct;
	}

	/**
	 * Sets the abstract.
	 *
	 * @param abstrct the new abstract
	 */
	public void setAbstract(final String abstrct) {
		this.abstrct = abstrct;
	}

}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.filerepository.metadata.ImageMetadata;

// TODO: Auto-generated Javadoc
/**
 * An {@link RepositoryImage} is an graphics file in one of the supported image formats (PNG and JPG). It extends the
 * {@link RepositoryFile} by including image-specific metadata defined in {@link ImageMetadata}.
 *
 * @author mobreza
 */
@Entity
@Table(name = "repositoryimage")
public class RepositoryImage extends RepositoryFile implements ImageMetadata {

	/** The width. */
	@Column(name = "width")
	private int width;

	/** The height. */
	@Column(name = "height")
	private int height;

	/** The orientation. */
	@Enumerated(EnumType.ORDINAL)
	private Orientation orientation;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.model.RepositoryFile#prePersist()
	 */
	@Override
	@PrePersist
	protected void prePersist() {
		// Don't forget the superclass!
		super.prePersist();
		updateOrientation();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.ImageMetadata#getWidth()
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/**
	 * Sets the width.
	 *
	 * @param width the new width
	 */
	public void setWidth(final int width) {
		this.width = width;
		updateOrientation();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.ImageMetadata#getHeight()
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(final int height) {
		this.height = height;
		updateOrientation();
	}

	/**
	 * See {@link ImageMetadata#getOrientation()}: Landscape = width is greater than height.
	 *
	 * @return the orientation
	 */
	@Override
	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * Sets the orientation.
	 *
	 * @param orientation the new orientation
	 */
	public void setOrientation(final Orientation orientation) {
		this.orientation = orientation;
	}

	/**
	 * Update orientation.
	 */
	private void updateOrientation() {
		orientation = width > height ? Orientation.LANDSCAPE : Orientation.PORTRAIT;
	}
}

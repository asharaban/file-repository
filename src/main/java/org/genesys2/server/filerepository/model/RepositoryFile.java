/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.genesys2.server.filerepository.metadata.BaseMetadata;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

// TODO: Auto-generated Javadoc
/**
 * The Class RepositoryFile.
 */
@Entity
@Table(name = "repositoryfile")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class RepositoryFile extends BaseEntity implements BaseMetadata {

	/** The uuid. */
	@Column(name = "uuid", unique = true, nullable = false, updatable = false, columnDefinition = "binary(16)")
	private UUID uuid;

	/** The path. */
	// Path in the repository
	@Column(name = "path", nullable = false)
	private String path;

	/** The original filename. */
	// Original filename
	@Column(name = "originalFilename", nullable = false)
	private String originalFilename;

	/** The extension. */
	// Extension based on mime type
	@Column(name = "extension")
	private String extension;

	/** The title. */
	@Column(name = "title")
	private String title;

	/** The subject. */
	@Column(name = "subject")
	private String subject;

	/** The description. */
	@Column(name = "description")
	private String description;

	/** The creator. */
	@Column(name = "creator")
	private String creator;

	/** The created. */
	@Column(name = "created")
	private String created;

	/** The rights holder. */
	@Column(name = "rightsHolder")
	private String rightsHolder;

	/** The access rights. */
	@Column(name = "accessRights")
	private String accessRights;

	/** The license. */
	@Column(name = "license")
	private String license;

	/** The mime type. */
	@Column(name = "mimeType")
	private String mimeType;

	/** The extent. */
	@Column(name = "extent")
	private String extent;

	/** The bibliographic citation. */
	@Column(name = "bibliographicCitation")
	private String bibliographicCitation;

	/** The created date. */
	@CreatedDate
	@Column(name = "createdDate")
	private Date createdDate;

	/** The last modified date. */
	@LastModifiedDate
	@Column(name = "lastModifiedDate")
	private Date lastModifiedDate;

	/**
	 * Pre persist.
	 */
	@PrePersist
	protected void prePersist() {
		if (uuid == null) {
			uuid = UUID.randomUUID();
		}
	}

	// TODO See if we can have a setter instead
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getIdentifier()
	 */
	// of this formula.
	@Override
	@Transient
	public String getIdentifier() {
		return "urn:uuid:" + uuid.toString();
	}

	/**
	 * Get the relative URL to the resource. As such, it cannot serve as an {@link #getIdentifier()} property.
	 *
	 * @return relative URL to the file resource
	 */
	@Transient
	public String getUrl() {
		final StringBuffer sb = new StringBuffer();
		sb.append(path).append(getFilename());
		return sb.toString();
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	// not persisted
	@Transient
	public String getFilename() {
		final StringBuffer sb = new StringBuffer();
		sb.append(uuid.toString());
		if (extension != null) {
			sb.append(extension);
		}
		return sb.toString();
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uuid the new uuid
	 */
	protected void setUuid(final UUID uuid) {
		this.uuid = uuid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getSubject()
	 */
	@Override
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param subject the new subject
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getCreator()
	 */
	@Override
	public String getCreator() {
		return creator;
	}

	/**
	 * Sets the creator.
	 *
	 * @param creator the new creator
	 */
	public void setCreator(final String creator) {
		this.creator = creator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getCreated()
	 */
	@Override
	public String getCreated() {
		return created;
	}

	/**
	 * Sets the created.
	 *
	 * @param created the new created
	 */
	public void setCreated(final String created) {
		this.created = created;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getRightsHolder()
	 */
	@Override
	public String getRightsHolder() {
		return rightsHolder;
	}

	/**
	 * Sets the rights holder.
	 *
	 * @param rightsHolder the new rights holder
	 */
	public void setRightsHolder(final String rightsHolder) {
		this.rightsHolder = rightsHolder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getAccessRights()
	 */
	@Override
	public String getAccessRights() {
		return accessRights;
	}

	/**
	 * Sets the access rights.
	 *
	 * @param accessRights the new access rights
	 */
	public void setAccessRights(final String accessRights) {
		this.accessRights = accessRights;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getLicense()
	 */
	@Override
	public String getLicense() {
		return license;
	}

	/**
	 * Sets the license.
	 *
	 * @param license the new license
	 */
	public void setLicense(final String license) {
		this.license = license;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getFormat()
	 */
	@Override
	public String getFormat() {
		return getMimeType();
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 *
	 * @param mimeType the new mime type
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getExtent()
	 */
	@Override
	public String getExtent() {
		return extent;
	}

	/**
	 * Sets the extent.
	 *
	 * @param extent the new extent
	 */
	public void setExtent(final String extent) {
		this.extent = extent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getBibliographicCitation()
	 */
	@Override
	public String getBibliographicCitation() {
		return bibliographicCitation;
	}

	/**
	 * Sets the bibliographic citation.
	 *
	 * @param bibliographicCitation the new bibliographic citation
	 */
	public void setBibliographicCitation(final String bibliographicCitation) {
		this.bibliographicCitation = bibliographicCitation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getDateSubmitted()
	 */
	@Override
	public Date getDateSubmitted() {
		return getCreatedDate();
	}

	/**
	 * Gets the created date.
	 *
	 * @return the created date
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the created date.
	 *
	 * @param createdDate the new created date
	 */
	public void setCreatedDate(final Date createdDate) {
		this.createdDate = createdDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.genesys2.server.filerepository.metadata.BaseMetadata#getModified()
	 */
	@Override
	public Date getModified() {
		return getLastModifiedDate();
	}

	/**
	 * Gets the last modified date.
	 *
	 * @return the last modified date
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * Sets the last modified date.
	 *
	 * @param lastModifiedDate the new last modified date
	 */
	public void setLastModifiedDate(final Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * Sets the original filename.
	 *
	 * @param originalFilename the new original filename
	 */
	public void setOriginalFilename(final String originalFilename) {
		final int dotIndex = originalFilename.indexOf(".");
		this.originalFilename = originalFilename;

		setExtension(dotIndex > 0 ? originalFilename.substring(dotIndex) : null);
	}

	/**
	 * Gets the original filename.
	 *
	 * @return the original filename
	 */
	public String getOriginalFilename() {
		return originalFilename;
	}

	/**
	 * Get file extension as provided in the original file.
	 *
	 * @return extension (including .) or null.
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Sets the extension.
	 *
	 * @param extension the new extension
	 */
	protected void setExtension(final String extension) {
		this.extension = extension;
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the new path
	 */
	public void setPath(final String path) {
		this.path = path;
	}
}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.persistence;

import java.util.List;
import java.util.UUID;

import org.genesys2.server.filerepository.model.RepositoryImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface RepositoryImagePersistence.
 */
@Repository
public interface RepositoryImagePersistence extends JpaRepository<RepositoryImage, Long> {

	/**
	 * Find by uuid.
	 *
	 * @param uuid the uuid
	 * @return the repository image
	 */
	RepositoryImage findByUuid(UUID uuid);

	/**
	 * Find by path.
	 *
	 * @param repositoryPath the repository path
	 * @return the list
	 */
	List<RepositoryImage> findByPath(String repositoryPath);
}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.config;

import java.io.File;

import org.genesys2.server.filerepository.service.BytesStorageService;
import org.genesys2.server.filerepository.service.RepositoryService;
import org.genesys2.server.filerepository.service.impl.FilesystemStorageServiceImpl;
import org.genesys2.server.filerepository.service.impl.RepositoryServiceImpl;
import org.genesys2.server.filerepository.service.impl.S3StorageServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceBeanConfig.
 */
@Configuration
public class ServiceBeanConfig {

	/**
	 * Bytes storage service.
	 *
	 * @return the bytes storage service
	 */
	@Bean(name = "bytesStorageService")
	public BytesStorageService bytesStorageService() {
		final File repoDir = new File(System.getProperty("java.io.tmpdir"));
		final FilesystemStorageServiceImpl storageService = new FilesystemStorageServiceImpl();
		storageService.setRepositoryBaseDirectory(repoDir);

		return storageService;
	}

	/**
	 * File repository service.
	 *
	 * @return the repository service
	 */
	@Bean
	public RepositoryService fileRepositoryService() {
		return new RepositoryServiceImpl();
	}

	/**
	 * Bytes storage service.
	 *
	 * @return the bytes storage service
	 */
	@Bean(name = "S3Storage")
	public BytesStorageService S3StorageService() {
		return new S3StorageServiceImpl();
	}

	/**
	 * Property sources placeholder configurer.
	 *
	 * @return the property sources placeholder configurer
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}

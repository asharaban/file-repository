/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.genesys2.server.filerepository.metadata.ImageMetadata.Orientation;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class RepositoryImageAddTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class RepositoryImageAddTest {

	/** The initial mime type. */
	private final String initialMimeType = "image/png";

	/** The initial path. */
	private final String initialPath = "/initial/" + System.currentTimeMillis() + "/";

	/** The initial extension. */
	private final String initialExtension = ".png";

	/** The initial original filename. */
	private final String initialOriginalFilename = "lena" + initialExtension;

	/** The Constant EMPTY_BYTES. */
	private static final byte[] EMPTY_BYTES = new byte[0];

	/** The file repo service. */
	@Autowired
	private RepositoryService fileRepoService;

	/**
	 * Test basic image submission to repository. Empty byte[] results in 0 width, 0 height
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void addImage() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException,
	NoSuchRepositoryFileException, IOException {
		final RepositoryImage repoImage = fileRepoService.addImage(initialPath, initialOriginalFilename,
				initialMimeType, EMPTY_BYTES, null);

		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 0, 0, Orientation.PORTRAIT);
		fileRepoService.removeFile(repoImage);
	}

	/**
	 * Test metadata submission to image repository.
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void addFileWithMetadata() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException,
	NoSuchRepositoryFileException, IOException {
		final RepositoryImage fileData = new RepositoryImage();

		final String bibliographicCitation = "bibliographicCitation";
		final String accessRights = "accessRights";
		final String created = "created";
		final String creator = "creator";
		final String description = "description";
		final String extension2 = ".extension2";
		final String extent = "extent";
		final String license = "license";
		final String mimeType2 = "mimeType2";
		final String originalFilename2 = "originalFilename2";
		final String rightsHolder = "rightsHolder";
		final String subject = "subject";
		final String title = "File Repository Requirements";

		// This test is to ensure they get overwritten in the service
		assertThat("mimeType2 must not match", mimeType2, not(equalTo(initialMimeType)));
		assertThat("originalFilename2 must not match", originalFilename2, not(equalTo(initialOriginalFilename)));
		assertThat("extension2 must not match", extension2, not(equalTo(initialExtension)));

		fileData.setAccessRights(accessRights);
		fileData.setBibliographicCitation(bibliographicCitation);
		fileData.setCreated(created);
		fileData.setCreator(creator);
		fileData.setDescription(description);
		fileData.setExtent(extent);
		fileData.setLicense(license);
		fileData.setMimeType(mimeType2);
		fileData.setOriginalFilename(originalFilename2);
		fileData.setRightsHolder(rightsHolder);
		fileData.setSubject(subject);
		fileData.setTitle(title);

		final RepositoryImage repoImage = fileRepoService.addImage(initialPath, initialOriginalFilename,
				initialMimeType, EMPTY_BYTES, fileData);

		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 0, 0, Orientation.PORTRAIT);
		FileRepositoryTestUtil.checkMetadata(repoImage, bibliographicCitation, accessRights, created, creator,
				description, extent, license, rightsHolder, subject, title);
		fileRepoService.removeFile(repoImage);
	}

	/**
	 * Test valid image data10x10.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void testValidImageData10x10() throws IOException, InvalidRepositoryPathException,
	InvalidRepositoryFileDataException, NoSuchRepositoryFileException {
		final byte[] image10x10 = FileRepositoryTestUtil.readImage("images/10x10.png");
		final RepositoryImage repoImage = fileRepoService.addImage(initialPath, initialOriginalFilename,
				initialMimeType, image10x10, null);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 7, 7, Orientation.PORTRAIT);
		fileRepoService.removeFile(repoImage);
	}

	/**
	 * Test valid image data11x10.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void testValidImageData11x10() throws IOException, InvalidRepositoryPathException,
	InvalidRepositoryFileDataException, NoSuchRepositoryFileException {
		final byte[] image10x10 = FileRepositoryTestUtil.readImage("images/11x10.png");
		final RepositoryImage repoImage = fileRepoService.addImage(initialPath, initialOriginalFilename,
				initialMimeType, image10x10, null);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 11, 10, Orientation.LANDSCAPE);
		fileRepoService.removeFile(repoImage);
	}

	/**
	 * Test valid image data10x11.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void testValidImageData10x11() throws IOException, InvalidRepositoryPathException,
	InvalidRepositoryFileDataException, NoSuchRepositoryFileException {
		final byte[] image10x10 = FileRepositoryTestUtil.readImage("images/10x11.png");
		final RepositoryImage repoImage = fileRepoService.addImage(initialPath, initialOriginalFilename,
				initialMimeType, image10x10, null);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 10, 11, Orientation.PORTRAIT);
		fileRepoService.removeFile(repoImage);
	}
}

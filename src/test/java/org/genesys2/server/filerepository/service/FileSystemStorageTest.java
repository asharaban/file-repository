/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.genesys2.server.filerepository.BytesStorageException;
import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.genesys2.server.filerepository.service.impl.FilesystemStorageServiceImpl;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class FileSystemStorageTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class FileSystemStorageTest {

	/** The Constant EMPTY_BYTES. */
	private static final byte[] EMPTY_BYTES = new byte[0];

	/** The Constant SOME_BYTES. */
	private static final byte[] SOME_BYTES = "somebytes".getBytes();

	/** The base dir. */
	private static File baseDir;

	/** The bytes storage. */
	private final BytesStorageService bytesStorage = new FilesystemStorageServiceImpl();

	/**
	 * Before tests.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void beforeTests() throws IOException {
		baseDir = File.createTempFile("fs" + System.currentTimeMillis(), "test");
		baseDir.delete();
		baseDir.mkdir();
		// System.err.println(baseDir.getAbsolutePath());
		final File marker = new File(baseDir, "FILEREPOSITORY");
		// System.err.println("Created " + marker.getAbsolutePath());
		marker.createNewFile();
	}

	/**
	 * After tests.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@AfterClass
	public static void afterTests() throws IOException {
		// System.err.println("Deleting " + baseDir.getAbsolutePath());
		FileUtils.deleteDirectory(baseDir);
		// System.err.println("Exists? " + baseDir.exists());
	}

	/**
	 * Instantiates a new file system storage test.
	 */
	public FileSystemStorageTest() {
		try {
			((FilesystemStorageServiceImpl) bytesStorage).setRepositoryBaseDirectory(baseDir);
			((FilesystemStorageServiceImpl) bytesStorage).afterPropertiesSet();
		} catch (final BytesStorageException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Test blank path.
	 *
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testBlankPath() throws FileNotFoundException, IOException {
		final File expectedFile = new File(baseDir, "filename1");
		bytesStorage.upsert("", "filename1", EMPTY_BYTES);

		assertThat("File not created", expectedFile.exists(), is(true));
		assertThat("Created file is not a file", expectedFile.isFile(), is(true));
		assertThat("File size does not match", expectedFile.length(), is(0l));

		bytesStorage.remove("", "filename1");
		assertThat("File still exists", expectedFile.exists(), is(false));
	}

	/**
	 * Test slash path.
	 *
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testSlashPath() throws FileNotFoundException, IOException {
		final String path = "/foo/";
		final String filename = "filename1";

		final File expectedFile = new File(baseDir, "foo/filename1");
		bytesStorage.upsert(path, filename, EMPTY_BYTES);

		assertThat("File not created", expectedFile.exists(), is(true));
		assertThat("Created file is not a file", expectedFile.isFile(), is(true));
		assertThat("File size does not match", expectedFile.length(), is(0l));

		bytesStorage.remove(path, filename);
		assertThat("File still exists", expectedFile.exists(), is(false));
	}

	/**
	 * Test escape path.
	 *
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test(expected = IOException.class)
	public void testEscapePath() throws FileNotFoundException, IOException {
		final String path = "/../";
		final String filename = "filename1";
		bytesStorage.upsert(path, filename, EMPTY_BYTES);
	}

	/**
	 * Test escape path2.
	 *
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test(expected = IOException.class)
	public void testEscapePath2() throws FileNotFoundException, IOException {
		final String path = "/../../";
		final String filename = "filename1";

		// File expectedFile = new File(baseDir, path + filename);
		// System.err.println("???" + expectedFile.getCanonicalPath());
		bytesStorage.upsert(path, filename, EMPTY_BYTES);
	}

	/**
	 * Test double slash path.
	 *
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testDoubleSlashPath() throws FileNotFoundException, IOException {
		final String path = "/foo/bar/";
		final String filename = "filename2";

		final File expectedFile = new File(baseDir, "foo/bar/filename2");
		bytesStorage.upsert(path, filename, EMPTY_BYTES);

		assertThat("File not created", expectedFile.exists(), is(true));
		assertThat("Created file is not a file", expectedFile.isFile(), is(true));
		assertThat("File size does not match", expectedFile.length(), is(0l));

		bytesStorage.remove(path, filename);
		assertThat("File still exists", expectedFile.exists(), is(false));
	}

	/**
	 * Test file contents.
	 *
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testFileContents() throws FileNotFoundException, IOException {
		final String path = "/foo/bar/";
		final String filename = "somecontents";

		final File expectedFile = new File(baseDir, "foo/bar/somecontents");
		bytesStorage.upsert(path, filename, SOME_BYTES);

		assertThat("File not created", expectedFile.exists(), is(true));
		assertThat("Created file is not a file", expectedFile.isFile(), is(true));
		assertThat("File size does not match", expectedFile.length(), is((long) SOME_BYTES.length));

		final byte[] readBytes = FileUtils.readFileToByteArray(expectedFile);
		assertThat("Bytes don't match", readBytes, equalTo(SOME_BYTES));

		bytesStorage.remove(path, filename);
		assertThat("File still exists", expectedFile.exists(), is(false));
	}

}

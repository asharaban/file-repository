/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import java.io.IOException;
import java.util.UUID;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.genesys2.server.filerepository.metadata.ImageMetadata.Orientation;
import org.genesys2.server.filerepository.model.RepositoryImage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class RepositoryImageUpdateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class RepositoryImageUpdateTest {

	/** The initial mime type. */
	private final String initialMimeType = "image/png";

	/** The initial path. */
	private final String initialPath = "/initial/" + System.currentTimeMillis() + "/";

	/** The initial extension. */
	private final String initialExtension = ".png";

	/** The initial original filename. */
	private final String initialOriginalFilename = "lena" + initialExtension;

	/** The Constant EMPTY_BYTES. */
	private static final byte[] EMPTY_BYTES = new byte[0];

	/** The file repo service. */
	@Autowired
	private RepositoryService fileRepoService;

	/** The file uuid. */
	private UUID fileUuid;

	/**
	 * Before test.
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Before
	public void beforeTest() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException {
		fileUuid = fileRepoService.addImage(initialPath, initialOriginalFilename, initialMimeType, EMPTY_BYTES, null)
				.getUuid();
	}

	/**
	 * After test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@After
	public void afterTest() throws NoSuchRepositoryFileException, IOException {
		fileRepoService.removeFile(fileRepoService.getFile(fileUuid));
	}

	/**
	 * Test many update image data.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void testManyUpdateImageData() throws IOException, InvalidRepositoryPathException,
	InvalidRepositoryFileDataException, NoSuchRepositoryFileException {
		final byte[] image10x10 = FileRepositoryTestUtil.readImage("images/10x10.png");
		final byte[] image11x10 = FileRepositoryTestUtil.readImage("images/11x10.png");
		final byte[] image10x11 = FileRepositoryTestUtil.readImage("images/10x11.png");

		RepositoryImage repoImage = (RepositoryImage) fileRepoService.getFile(fileUuid);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 0, 0, Orientation.PORTRAIT);

		repoImage = fileRepoService.updateBytes(repoImage, "image/png", image10x10);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 7, 7, Orientation.PORTRAIT);

		repoImage = fileRepoService.updateBytes(repoImage, "image/png", image11x10);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 11, 10, Orientation.LANDSCAPE);

		repoImage = fileRepoService.updateBytes(repoImage, "image/png", image10x11);
		FileRepositoryTestUtil.checkImage(repoImage, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType, 10, 11, Orientation.PORTRAIT);
	}
}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

/**
 * The Class S3StorageServiceTest.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class S3StorageServiceTest {

	/** The Constant SOME_BYTES. */
	private static final byte[] SOME_BYTES = "Test string".getBytes();

	/** The Constant PATH. */
	private static final String PATH = "/temp/";

	/** The Constant FILENAME. */
	private static final String FILENAME = "test.txt";
	
	/** The bytes storage. */
	@Autowired
	@Qualifier("S3Storage")
	BytesStorageService bytesStorageService;

	/**
	 * Test add file.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test
	public void testAddFile() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert(PATH, FILENAME, SOME_BYTES);

		final byte[] response = bytesStorageService.get(PATH, FILENAME);
		assertThat("File bytes length is different", response.length, is(SOME_BYTES.length));
		assertArrayEquals("File bytes don't match", SOME_BYTES, response);

		bytesStorageService.remove(PATH, FILENAME);
	}

	/**
	 * Test remove file.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = HttpClientErrorException.class)
	public void testRemoveFile() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert(PATH, FILENAME, SOME_BYTES);

		bytesStorageService.remove(PATH, FILENAME);

		bytesStorageService.get(PATH, FILENAME);
	}

	/**
	 * Test update file.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test
	public void testUpdateFile() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert(PATH, FILENAME, SOME_BYTES);

		byte[] response = bytesStorageService.get(PATH, FILENAME);
		assertArrayEquals("File bytes don't match", SOME_BYTES, response);

		final byte[] bytes = new byte[] { 1, 2, 3 };
		bytesStorageService.upsert(PATH, FILENAME, bytes);

		response = bytesStorageService.get(PATH, FILENAME);
		assertArrayEquals("File bytes don't match", bytes, response);

		bytesStorageService.remove(PATH, FILENAME);
	}

	/**
	 * Test upsert method on null bytes.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertNullBytes() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert(PATH, FILENAME, null);
	}

	/**
	 * Test upsert without first slash.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertWithoutFirstSlash() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert("test/", FILENAME, SOME_BYTES);
	}

	/**
	 * Test upsert without last slash.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertWithoutLastSlash() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert("/test", FILENAME, SOME_BYTES);
	}

	/**
	 * Test upsert with double slash.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertWithDoubleSlash() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert("/test//test/", FILENAME, SOME_BYTES);
	}

	/**
	 * Test upsert with null path.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertNullPath() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert(null, FILENAME, SOME_BYTES);
	}

	/**
	 * Test upsert with blank path.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertBlankPath() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert(" ", FILENAME, SOME_BYTES);
	}

	/**
	 * Test upsert with null file name.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidUpsertNullFilename() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.upsert("/test/", null, SOME_BYTES);
	}

	/**
	 * Test remove without first slash.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidRemoveWithoutFirstSlash() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.remove("test/", FILENAME);
	}

	/**
	 * Test remove without last slash.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidRemoveWithoutLastSlash() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.remove("/test", FILENAME);
	}

	/**
	 * Test remove with double slash.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidRemoveWithDoubleSlash() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.remove("/test//test/", FILENAME);
	}

	/**
	 * Test remove with null path.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidRemoveNullPath() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.remove(null, FILENAME);
	}

	/**
	 * Test remove with blank path.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidRemoveBlankPath() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.remove(" ", FILENAME);
	}

	/**
	 * Test remove with null file name.
	 *
	 * @throws IOException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	@Test(expected = IOException.class)
	public void invalidRemoveNullFilename() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
		bytesStorageService.remove("/test/", null);
	}
}

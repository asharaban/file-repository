/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import java.io.IOException;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class FileRepositoryExtensionTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class FileRepositoryExtensionTest {

	/** The initial mime type. */
	private final String initialMimeType = "text/plain";

	/** The initial path. */
	private final String initialPath = "/initial/" + System.currentTimeMillis() + "/";

	/** The initial extension. */
	private final String initialExtension = ".txt";

	/** The initial original filename. */
	private final String initialOriginalFilename = "initial" + initialExtension;

	/** The Constant EMPTY_BYTES. */
	private static final byte[] EMPTY_BYTES = new byte[0];

	/** The file repo service. */
	@Autowired
	private RepositoryService fileRepoService;

	/**
	 * Check that extension is generated from originalFilename.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void extensionIsAssigned() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException, IOException {
		final RepositoryFile repoFile = fileRepoService.addFile(initialPath, initialOriginalFilename, initialMimeType,
				EMPTY_BYTES, null);
		FileRepositoryTestUtil.checkFile(repoFile, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);

		final String extension = ".extension2";
		final String originalFilename = "originalFilename2" + extension;
		updateAndCheck(repoFile, originalFilename, extension);

		fileRepoService.removeFile(repoFile);
	}

	/**
	 * Extension is null on add.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void extensionIsNullOnAdd() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException, IOException {
		final RepositoryFile repoFile = fileRepoService.addFile(initialPath, "no extension in file", initialMimeType,
				EMPTY_BYTES, null);
		FileRepositoryTestUtil.checkFile(repoFile, initialPath, "no extension in file", null, initialMimeType);
		fileRepoService.removeFile(repoFile);
	}

	/**
	 * Extension is null on update.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void extensionIsNullOnUpdate() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException, IOException {
		final RepositoryFile repoFile = fileRepoService.addFile(initialPath, initialOriginalFilename, initialMimeType,
				EMPTY_BYTES, null);
		updateAndCheck(repoFile, "no extension here", null);
		fileRepoService.removeFile(repoFile);
	}

	/**
	 * Extension dot file is null on add.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void extensionDotFileIsNullOnAdd() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException, IOException {
		final RepositoryFile repoFile = fileRepoService.addFile(initialPath, ".gitignore", initialMimeType,
				EMPTY_BYTES, null);
		FileRepositoryTestUtil.checkFile(repoFile, initialPath, ".gitignore", null, initialMimeType);
		fileRepoService.removeFile(repoFile);
	}

	/**
	 * Extension dot file is null on update.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void extensionDotFileIsNullOnUpdate() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException, IOException {
		final RepositoryFile repoFile = fileRepoService.addFile(initialPath, initialOriginalFilename, initialMimeType,
				EMPTY_BYTES, null);
		updateAndCheck(repoFile, ".gitignore", null);
		fileRepoService.removeFile(repoFile);
	}

	/**
	 * Update and check.
	 *
	 * @param repoFile the repo file
	 * @param originalFilename the original filename
	 * @param expectedExtension the expected extension
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	private void updateAndCheck(final RepositoryFile repoFile, final String originalFilename,
			final String expectedExtension) throws NoSuchRepositoryFileException {

		FileRepositoryTestUtil.checkFile(repoFile, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);

		repoFile.setOriginalFilename(originalFilename);
		fileRepoService.updateFile(repoFile);

		FileRepositoryTestUtil.checkFile(repoFile, initialPath, originalFilename, expectedExtension, initialMimeType);

	}

}

/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class FileRepositoryAddTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class FileRepositoryAddTest {

	/** The Constant SOME_BYTES. */
	private static final byte[] SOME_BYTES = "filecontents".getBytes();

	/** The Constant EMPTY_BYTES. */
	private static final byte[] EMPTY_BYTES = new byte[0];

	/** The file repo service. */
	@Autowired
	private RepositoryService fileRepoService;

	/**
	 * Test basic submission to file repository.
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test
	public void addFile() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException {
		final String path = "/test1/test2/";
		final String mimeType = "text/plain;charset=UTF-8";
		final String extension = ".txt";
		final String originalFilename = "requirements" + extension;

		final RepositoryFile repoFile = fileRepoService.addFile(path, originalFilename, mimeType, SOME_BYTES, null);

		FileRepositoryTestUtil.checkFile(repoFile, path, originalFilename, extension, mimeType);
	}

	/**
	 * Test metadata submission to file repository.
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test
	public void addFileWithMetadata() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException {
		final String path = "/test1/test2/";
		final String mimeType = "text/plain;charset=UTF-8";
		final String extension = ".txt";
		final String originalFilename = "requirements" + extension;

		final RepositoryFile fileData = new RepositoryFile();

		final String bibliographicCitation = "bibliographicCitation";
		final String accessRights = "accessRights";
		final String created = "created";
		final String creator = "creator";
		final String description = "description";
		final String extension2 = ".extension2";
		final String extent = "extent";
		final String license = "license";
		final String mimeType2 = "mimeType2";
		final String originalFilename2 = "originalFilename2";
		final String rightsHolder = "rightsHolder";
		final String subject = "subject";
		final String title = "File Repository Requirements";

		// This test is to ensure they get overwritten in the service
		assertThat("mimeType2 must not match", mimeType2, not(equalTo(mimeType)));
		assertThat("originalFilename2 must not match", originalFilename2, not(equalTo(originalFilename)));
		assertThat("extension2 must not match", extension2, not(equalTo(extension)));

		fileData.setAccessRights(accessRights);
		fileData.setBibliographicCitation(bibliographicCitation);
		fileData.setCreated(created);
		fileData.setCreator(creator);
		fileData.setDescription(description);
		fileData.setExtent(extent);
		fileData.setLicense(license);
		fileData.setMimeType(mimeType2);
		fileData.setOriginalFilename(originalFilename2);
		fileData.setRightsHolder(rightsHolder);
		fileData.setSubject(subject);
		fileData.setTitle(title);

		final RepositoryFile repoFile = fileRepoService.addFile(path, originalFilename, mimeType, SOME_BYTES, fileData);

		FileRepositoryTestUtil.checkFile(repoFile, path, originalFilename, extension, mimeType);
		FileRepositoryTestUtil.checkMetadata(repoFile, bibliographicCitation, accessRights, created, creator,
				description, extent, license, rightsHolder, subject, title);
	}

	/**
	 * Base data missing originalFilename.
	 *
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryFileDataException.class)
	public void invalidMissingOriginalFilename() throws InvalidRepositoryFileDataException,
			InvalidRepositoryPathException {
		fileRepoService.addFile("/valid/path/", null, "mimeType", FileRepositoryAddTest.EMPTY_BYTES, null);
	}

	/**
	 * Base data missing mimeType.
	 *
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryFileDataException.class)
	public void invalidMissingMimeType() throws InvalidRepositoryFileDataException, InvalidRepositoryPathException {
		fileRepoService.addFile("/valid/path/", "orignalFilename.txt", null, FileRepositoryAddTest.EMPTY_BYTES, null);
	}

	/**
	 * Base data missing bytes.
	 *
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryFileDataException.class)
	public void invalidMissingBytes() throws InvalidRepositoryFileDataException, InvalidRepositoryPathException {
		fileRepoService.addFile("/valid/path/", "orignalFilename.txt", "mimeType", null, null);
	}

	/**
	 * Null path destination test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidAddNullPath() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException {
		fileRepoService.addFile(null, "originalFilename.txt", "mimeType", FileRepositoryAddTest.EMPTY_BYTES, null);
	}

	/**
	 * Blank path destination test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidAddBlankPath() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException {
		fileRepoService.addFile("  ", "originalFilename.txt", "mimeType", FileRepositoryAddTest.EMPTY_BYTES, null);
	}

	/**
	 * Invalid double // in path test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidAddDoubleSlashPath() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException {
		fileRepoService.addFile("/double//slash/", "originalFilename.txt", "mimeType",
				FileRepositoryAddTest.EMPTY_BYTES, null);
	}

	/**
	 * Path must start with / test.
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidAddMissingStartSlash() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException {
		fileRepoService.addFile("invalidpath/here/", "originalFilename.txt", "mimeType",
				FileRepositoryAddTest.EMPTY_BYTES, null);
	}

	/**
	 * Path must end with / test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidAddMissingEndSlash() throws NoSuchRepositoryFileException, InvalidRepositoryPathException,
			InvalidRepositoryFileDataException {
		fileRepoService.addFile("/invalidpath/here", "originalFilename.txt", "mimeType",
				FileRepositoryAddTest.EMPTY_BYTES, null);
	}

}

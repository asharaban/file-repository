/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.config.DatabaseConfig;
import org.genesys2.server.filerepository.config.ServiceBeanConfig;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class FileRepositoryUpdateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ServiceBeanConfig.class, DatabaseConfig.class })
public class FileRepositoryUpdateTest {

	/** The initial mime type. */
	private final String initialMimeType = "text/plain";

	/** The initial path. */
	private final String initialPath = "/initial/" + System.currentTimeMillis() + "/";

	/** The initial extension. */
	private final String initialExtension = ".txt";

	/** The initial original filename. */
	private final String initialOriginalFilename = "initial" + initialExtension;

	/** The file repo service. */
	@Autowired
	private RepositoryService fileRepoService;

	/** The file uuid. */
	private UUID fileUuid;

	/**
	 * Before test.
	 *
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 * @throws InvalidRepositoryFileDataException the invalid repository file data exception
	 */
	@Before
	public void beforeTest() throws InvalidRepositoryPathException, InvalidRepositoryFileDataException {
		fileUuid = fileRepoService.addFile(initialPath, initialOriginalFilename, initialMimeType, "initial".getBytes(),
				null).getUuid();
	}

	/**
	 * After test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@After
	public void afterTest() throws NoSuchRepositoryFileException, IOException {
		fileRepoService.removeFile(fileRepoService.getFile(fileUuid));
	}

	/**
	 * Test missing file.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test(expected = NoSuchRepositoryFileException.class)
	public void fetchFileNoSuchFile() throws NoSuchRepositoryFileException {
		fileRepoService.getFile(UUID.randomUUID());
	}

	/**
	 * Test basic submission to file repository.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void fetchFileByUuid() throws NoSuchRepositoryFileException {
		final RepositoryFile repoFile = fileRepoService.getFile(fileUuid);

		FileRepositoryTestUtil.checkFile(repoFile, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);
	}

	/**
	 * Test basic submission to file repository.
	 */
	@Test
	public void fetchFilesByPath() {
		final List<RepositoryFile> repoFiles = fileRepoService.getFiles(initialPath);

		assertThat("List should have one element", repoFiles.size(), is(1));

		final RepositoryFile repoFile = repoFiles.get(0);
		FileRepositoryTestUtil.checkFile(repoFile, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);
		FileRepositoryTestUtil.assertMetadataIsBlank(repoFile);
	}

	/**
	 * Change of originalFilename must update extension.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void originalFilenameChangeUpdatesExtension() throws NoSuchRepositoryFileException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);
		final String extension = ".png";
		final String originalFilename = "originalFilename2" + extension;
		fileData.setOriginalFilename(originalFilename);
		fileData = fileRepoService.updateFile(fileData);
		assertThat("Extension was not updated", fileData.getExtension(), equalTo(extension));
	}

	/**
	 * Add metadata to existing file.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 */
	@Test
	public void addMetadataToFile() throws NoSuchRepositoryFileException {
		final RepositoryFile fileData = fileRepoService.getFile(fileUuid);

		FileRepositoryTestUtil.checkFile(fileData, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);
		FileRepositoryTestUtil.assertMetadataIsBlank(fileData);

		final String bibliographicCitation = "bibliographicCitation";
		final String accessRights = "accessRights";
		final String created = "created";
		final String creator = "creator";
		final String description = "description";
		final String extension2 = ".extension2";
		final String extent = "extent";
		final String license = "license";
		final String mimeType2 = "mimeType2";
		final String originalFilename2 = "originalFilename2" + extension2;
		final String rightsHolder = "rightsHolder";
		final String subject = "subject";
		final String title = "File Repository Requirements";

		fileData.setAccessRights(accessRights);
		fileData.setBibliographicCitation(bibliographicCitation);
		fileData.setCreated(created);
		fileData.setCreator(creator);
		fileData.setDescription(description);
		fileData.setExtent(extent);
		fileData.setLicense(license);
		fileData.setMimeType(mimeType2);
		fileData.setOriginalFilename(originalFilename2);
		fileData.setRightsHolder(rightsHolder);
		fileData.setSubject(subject);
		fileData.setTitle(title);

		final RepositoryFile repoFile = fileRepoService.updateFile(fileData);

		// Entity test
		assertThat("RepositoryFile cannot be null", repoFile, notNullValue());
		assertThat("RepositoryFile#UUID cannot be null", repoFile.getUuid(), notNullValue());
		assertThat("UUIDs must match", repoFile.getUuid(), equalTo(fileData.getUuid()));

		FileRepositoryTestUtil.checkFile(repoFile, initialPath, originalFilename2, extension2, mimeType2);
		FileRepositoryTestUtil.checkMetadata(repoFile, bibliographicCitation, accessRights, created, creator,
				description, extent, license, rightsHolder, subject, title);
	}

	/**
	 * Change the path of the repository file.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test
	public void moveFile() throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);

		FileRepositoryTestUtil.checkFile(fileData, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);
		FileRepositoryTestUtil.assertMetadataIsBlank(fileData);

		// Move
		final String newPath = "/new/" + System.currentTimeMillis() + "/";
		fileData = fileRepoService.moveFile(fileData, newPath);
		FileRepositoryTestUtil.checkFile(fileData, newPath, initialOriginalFilename, initialExtension, initialMimeType);

		// Move back
		fileData = fileRepoService.moveFile(fileData, initialPath);
		FileRepositoryTestUtil.checkFile(fileData, initialPath, initialOriginalFilename, initialExtension,
				initialMimeType);
	}

	/**
	 * Null path destination test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidMoveNullPath() throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);
		fileData = fileRepoService.moveFile(fileData, null);
	}

	/**
	 * Blank path destination test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidMoveBlankPath() throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);
		fileData = fileRepoService.moveFile(fileData, "  ");
	}

	/**
	 * Invalid double // in path test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidMoveDoubleSlashPath() throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);
		fileData = fileRepoService.moveFile(fileData, "/fo//bar/");
	}

	/**
	 * Path must start with / test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidMoveMissingStartSlash() throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);
		fileData = fileRepoService.moveFile(fileData, "validpath/here/");
	}

	/**
	 * Path must end with / test.
	 *
	 * @throws NoSuchRepositoryFileException the no such repository file exception
	 * @throws InvalidRepositoryPathException the invalid repository path exception
	 */
	@Test(expected = InvalidRepositoryPathException.class)
	public void invalidMoveMissingEndSlash() throws NoSuchRepositoryFileException, InvalidRepositoryPathException {
		RepositoryFile fileData = fileRepoService.getFile(fileUuid);
		fileData = fileRepoService.moveFile(fileData, "/validpath/here");
	}

}

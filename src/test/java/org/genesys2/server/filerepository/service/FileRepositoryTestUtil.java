/*
 * Copyright 2016 Global Crop Diversity Trust, www.croptrust.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.filerepository.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.genesys2.server.filerepository.metadata.ImageMetadata.Orientation;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.genesys2.server.filerepository.model.RepositoryImage;

// TODO: Auto-generated Javadoc
/**
 * The Class FileRepositoryTestUtil.
 */
public class FileRepositoryTestUtil {

	/**
	 * Check file.
	 *
	 * @param repoFile the repo file
	 * @param path the path
	 * @param originalFilename the original filename
	 * @param extension the extension
	 * @param mimeType the mime type
	 */
	static void checkFile(final RepositoryFile repoFile, final String path, final String originalFilename,
			final String extension, final String mimeType) {
		assertThat("RepositoryFile cannot be null", repoFile, notNullValue());
		assertThat("RepositoryFile#UUID cannot be null", repoFile.getUuid(), notNullValue());
		assertThat("RepositoryFile#originalFilename doesn't match", repoFile.getOriginalFilename(),
				equalTo(originalFilename));
		assertThat("RepositoryFile#extension doesn't match", repoFile.getExtension(), equalTo(extension));
		assertThat("RepositoryFile#mimeType doesn't match", repoFile.getMimeType(), equalTo(mimeType));
		assertThat("RepositoryFile#URL doesn't match", repoFile.getUrl(), equalTo(path + repoFile.getUuid().toString()
				+ (extension != null ? extension : "")));
	}

	/**
	 * Check metadata.
	 *
	 * @param repoFile the repo file
	 * @param bibliographicCitation the bibliographic citation
	 * @param accessRights the access rights
	 * @param created the created
	 * @param creator the creator
	 * @param description the description
	 * @param extent the extent
	 * @param license the license
	 * @param rightsHolder the rights holder
	 * @param subject the subject
	 * @param title the title
	 */
	static void checkMetadata(final RepositoryFile repoFile, final String bibliographicCitation,
			final String accessRights, final String created, final String creator, final String description,
			final String extent, final String license, final String rightsHolder, final String subject,
			final String title) {
		assertThat("RepositoryFile#accessRights doesn't match", repoFile.getAccessRights(), equalTo(accessRights));
		assertThat("RepositoryFile#bibliographicCitation doesn't match", repoFile.getBibliographicCitation(),
				equalTo(bibliographicCitation));
		assertThat("RepositoryFile#created doesn't match", repoFile.getCreated(), equalTo(created));
		assertThat("RepositoryFile#creator doesn't match", repoFile.getCreator(), equalTo(creator));
		assertThat("RepositoryFile#description doesn't match", repoFile.getDescription(), equalTo(description));
		assertThat("RepositoryFile#extent doesn't match", repoFile.getExtent(), equalTo(extent));
		assertThat("RepositoryFile#license doesn't match", repoFile.getLicense(), equalTo(license));
		assertThat("RepositoryFile#rightsHolder doesn't match", repoFile.getRightsHolder(), equalTo(rightsHolder));
		assertThat("RepositoryFile#subject doesn't match", repoFile.getSubject(), equalTo(subject));
		assertThat("RepositoryFile#title doesn't match", repoFile.getTitle(), equalTo(title));
	}

	/**
	 * Assert metadata is blank.
	 *
	 * @param fileData the file data
	 */
	static void assertMetadataIsBlank(final RepositoryFile fileData) {
		assertThat("RepositoryFile#accessRights doesn't match", fileData.getAccessRights(), nullValue());
		assertThat("RepositoryFile#bibliographicCitation doesn't match", fileData.getBibliographicCitation(),
				nullValue());
		assertThat("RepositoryFile#created doesn't match", fileData.getCreated(), nullValue());
		assertThat("RepositoryFile#creator doesn't match", fileData.getCreator(), nullValue());
		assertThat("RepositoryFile#description doesn't match", fileData.getDescription(), nullValue());
		assertThat("RepositoryFile#extent doesn't match", fileData.getExtent(), nullValue());
		assertThat("RepositoryFile#license doesn't match", fileData.getLicense(), nullValue());
		assertThat("RepositoryFile#rightsHolder doesn't match", fileData.getRightsHolder(), nullValue());
		assertThat("RepositoryFile#subject doesn't match", fileData.getSubject(), nullValue());
		assertThat("RepositoryFile#title doesn't match", fileData.getTitle(), nullValue());
	}

	/**
	 * Check image.
	 *
	 * @param imageData the image data
	 * @param path the path
	 * @param originalFilename the original filename
	 * @param extension the extension
	 * @param mimeType the mime type
	 * @param width the width
	 * @param height the height
	 * @param orientation the orientation
	 */
	public static void checkImage(final RepositoryImage imageData, final String path, final String originalFilename,
			final String extension, final String mimeType, final int width, final int height,
			final Orientation orientation) {
		checkFile(imageData, path, originalFilename, extension, mimeType);

		assertThat("RepositoryImage#width doesn't match", imageData.getWidth(), is(width));
		assertThat("RepositoryImage#height doesn't match", imageData.getHeight(), is(height));
		assertThat("RepositoryImage#orientation doesn't match", imageData.getOrientation(), is(orientation));

	}

	/**
	 * Read image.
	 *
	 * @param resourcePath the resource path
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static byte[] readImage(final String resourcePath) throws IOException {
		try (InputStream is = FileRepositoryTestUtil.class.getClassLoader().getResourceAsStream(resourcePath)) {
			return IOUtils.toByteArray(is);
		}
	}
}

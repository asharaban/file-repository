
# Simple File Repository

**file-repository** library provides basic file repository
functionality, using a basic set of DublinCore metadata.



## Using file-repository

Add the dependency to your pom.xml:

	<dependency>
	 	<groupId>org.genesys-pgr</groupId>
	 	<artifactId>file-repository</artifactId>
	 	<version>X.Y.Z</version>
	</dependency>

## Using the S3 as bytes storage

Adjust the configuration in application.properties file and use the S3StorageServiceImpl as 
the BytesStorageService of the RepositoryServiceImpl.

	#AWS parameter values
	s3.accessKey=AKIAJQCIQYDRXEXAMPLE
	s3.secretKey=dCWEXAMPLEXQMLl/d+U2EdEXAMPLEcIy7HONnPDR
	s3.bucket=bucketname
	s3.region=eu-west-1

Make sure these credentials have read, write and delete permissions on the selected bucket.
